<?php
    //crear una clase
     class Perfiles extends CI_Controller{
          //contructor
          public function __construct(){
            //llamamos al contructor
          parent::__construct();
          //carga el modelo
          $this->load->model("perfil");
        }
public function index(){
  //muestra los datos en el index
  //LOS DATOS LLEGAN A LA VISTA
  $data["listadoPerfiles"]=$this->perfil->consultarTodos();
  $this->load->view("header");
  $this->load->view("perfiles/index",$data);
  $this->load->view("footer");
}
public function nuevo(){
  $this->load->view("header");
  $this->load->view("perfiles/nuevo");
  $this->load->view("footer");
}
public function editar($id_perfil){
  $data["perfil"]=$this->perfil->consultarPorId($id_perfil);
  $this->load->view("header");
  $this->load->view("perfiles/editar",$data);
  $this->load->view("footer");
}
public function procesarActualizacion(){
  $id_perfil=$this->input->post("id_perfil");
  $datosPerfilEditado=array(
    "nombre_perfil"=>$this->input->post("nombre_perfil"),
    "descripcion_perfil"=>$this->input->post("descripcion_perfil")
  );
// //logica de negocio para subir la fotografia del cliente
//   $this->load->library("upload");//carga de la libreria de subida de
//   $nombreTemporal="foto_cliente_".time()."_".rand(1,5000);
//   $config["file_name"]=$nombreTemporal;
//   $config["upload_path"]=APPPATH.'../uploads/clientes/';
//   $config["allowed_types"]="jpeg|jpg|png";
//   $config["max_size"]=2*1024;//2MB
//   $this->upload->initialize($config);
//   //codigo para subir el archivo y guardar el nombre en la BDD
//   if($this->upload->do_upload("foto_cli")){
//     $dataSubida=$this->upload->data();
//     $datosClienteEditado["foto_cli"]=$dataSubida["file_name"];
//   }


  if($this->perfil->actualizar($id_perfil,$datosPerfilEditado)){
    redirect("perfiles/index");
  }else{
    echo "ERROR AL ACTUALIZAR";
  }
}
public function guardarPerfil(){
      $datosNuevoPerfil=array(
        "nombre_perfil"=>$this->input->post("nombre_perfil"),
        "descripcion_perfil"=>$this->input->post("descripcion_perfil")
      );
      // //logica de negocio para subir la fotografia del cliente
      //   $this->load->library("upload");//carga de la libreria de subida de
      //   $nombreTemporal="foto_cliente_".time()."_".rand(1,5000);
      //   $config["file_name"]=$nombreTemporal;
      //   $config["upload_path"]=APPPATH.'../uploads/clientes/';
      //   $config["allowed_types"]="jpeg|jpg|png";
      //   $config["max_size"]=2*1024;//2MB
      //   $this->upload->initialize($config);
      //   //codigo para subir el archivo y guardar el nombre en la BDD
      //   if($this->upload->do_upload("foto_cli")){
      //     $dataSubida=$this->upload->data();
      //     $datosNuevoCliente["foto_cli"]=$dataSubida["file_name"];
      //  }
      if ($this->perfil->insertar($datosNuevoPerfil)) {
        $this->session->set_flashdata('confirmacion',"Genero insertado exitosamente.");
      }else {
        $this->session->set_flashdata('error',"Error al procesar, intente nuevamente.");
      }
      redirect("perfiles/index");
    }


    public function procesarEliminacion($id_perfil){
        if($this->perfil->eliminar($id_perfil)){
          redirect("perfiles/index");
      }else{
        echo "Error al Eliminar";
      }

  }


}
 ?>
