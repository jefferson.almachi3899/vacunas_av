<?php
    class Personas extends CI_Controller{
        public function __construct(){
          parent::__construct();
          $this->load->model('persona');
          $this->load->model("genero");
        }

        public function index(){
          $data["listadoPersonas"]=$this->persona->consultarTodos();
          $this->load->view('header');
          $this->load->view('personas/index',$data);
          $this->load->view('footer');
        }

        public function nuevo(){
          $data["listadoGeneros"]=$this->genero->consultarTodos();
          $this->load->view('header');
          $this->load->view('personas/nuevo',$data);
          $this->load->view('footer');
        }
        public function editar($id_per){
          $data["listadoGeneros"]=$this->genero->consultarTodos();
          $data["persona"]=$this->persona->consultarPorId($id_per);
          $this->load->view('header');
          $this->load->view('personas/editar',$data);
          $this->load->view('footer');
        }
        public function procesarActualizacion(){
          $id_per=$this->input->post("id_per");
          $datosPersonaEditado=array(
            "nombre_per"=>$this->input->post("nombre_per"),
            "apellido_per"=>$this->input->post("apellido_per"),
            "identificacion_per"=>$this->input->post("identificacion_per"),
            "fk_id_gen"=>$this->input->post("fk_id_gen"),
          );
          $this->load->library("upload");
          $new_name = "foto_per_" . time() . "_" . rand(1, 5000);
          $config["file_name"]=$nombreTemporal;
          $config["upload_path"]=APPPATH.'../uploads/personas/';
          $config['allowed_types']        = 'jpeg|jpg|png';
          $config['max_size']             = 4*1024;
          $this->upload->initialize($config);

          if ($this->upload->do_upload("foto_per")) {
            $dataSubida = $this->upload->data();
            $datosPersonaEditado["foto_per"] = $dataSubida['file_name'];
          }


          if ($this->persona->actualizar($id_per,$datosPersonaEditado)) {
            //echo "INSERCION EXITOSA";
            redirect("personas/index");
          } else {
            echo "ERROR AL ACTUALIZAR";
          }
        }

        //registro de usuarios
        public function guardarPersona(){
          $datosNuevoPersona=array(
            "nombre_per"=>$this->input->post("nombre_per"),
            "apellido_per"=>$this->input->post("apellido_per"),
            "identificacion_per"=>$this->input->post("identificacion_per"),
            "fk_id_gen"=>$this->input->post("fk_id_gen"),
          );

          $this->load->library("upload");
          $nombreTemporal="foto_per_".time()."_".rand(1,5000);
          $config["file_name"]=$nombreTemporal;
          $config["upload_path"]=APPPATH.'../uploads/personas/';
          $config["allowed_types"]="jpeg|jpg|png";
          $config["max_size"]=2*1024;//2mb
          $this->upload->initialize($config);

          if ($this->upload->do_upload("foto_per")) {
            $dataSubida=$this->upload->data();
            $datosNuevoPersona["foto_per"]=$dataSubida["file_name"];
          }

          if ($this->persona->insertar($datosNuevoPersona)) {
              $this->session->set_flashdata("confirmacion","PERSONA INSERTADO EXITOSAMENTE.");
          } else {
            $this->session->set_flashdata("error","ERROR AL PROCESAR, INTENTE NUEVAMENTE.");
          }
          redirect("personas/index");

        }
        function procesarEliminacion($id_per){
            // code...
            if($this->persona->eliminar($id_per)){
              redirect("personas/index");
            }else{
              echo "Error al eliminar";
            }

    }
    }//cierre de la clase
 ?>
