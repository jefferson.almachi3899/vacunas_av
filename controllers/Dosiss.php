<?php
    class Dosiss extends CI_Controller{
        public function __construct(){
          parent::__construct();
          $this->load->model('dosis');
          $this->load->model("vacuna");
          $this->load->model("persona");
        }

        public function index(){
          $data["listadoDosiss"]=$this->dosis->consultarTodos();
          $this->load->view('header');
          $this->load->view('dosiss/index',$data);
          $this->load->view('footer');
        }

        public function nuevo(){
          $data["listadoVacunas"]=$this->vacuna->consultarTodos();
          $data["listadoPersonas"]=$this->persona->consultarTodos();
          $this->load->view('header');
          $this->load->view('dosiss/nuevo',$data);
          $this->load->view('footer');
        }
        public function editar($id_dos){
          $data["listadoVacunas"]=$this->vacuna->consultarTodos();
          $data["listadoPersonas"]=$this->persona->consultarTodos();
          $data["dosis"]=$this->dosis->consultarPorId($id_dos);
          $this->load->view('header');
          $this->load->view('dosiss/editar',$data);
          $this->load->view('footer');
        }
        public function procesarActualizacion(){
          $id_dos=$this->input->post("id_dos");
          $datosDosisEditado=array(
            "lugar_dos"=>$this->input->post("lugar_dos"),
            "numero_dos"=>$this->input->post("numero_dos"),
            "vacunador_dos"=>$this->input->post("vacunador_dos"),
            "fecha_dos"=>$this->input->post("fecha_dos"),
            "fk_id_vac"=>$this->input->post("fk_id_vac"),
            "fk_id_per"=>$this->input->post("fk_id_per"),
          );
          if ($this->dosis->actualizar($id_dos,$datosDosisEditado)) {
            //echo "INSERCION EXITOSA";
            redirect("dosiss/index");
          } else {
            echo "ERROR AL ACTUALIZAR";
          }
        }

        //registro de usuarios
        public function guardarDosis(){
          $datosNuevoDosis=array(
            "lugar_dos"=>$this->input->post("lugar_dos"),
            "numero_dos"=>$this->input->post("numero_dos"),
            "vacunador_dos"=>$this->input->post("vacunador_dos"),
            "fecha_dos"=>$this->input->post("fecha_dos"),
            "fk_id_vac"=>$this->input->post("fk_id_vac"),
            "fk_id_per"=>$this->input->post("fk_id_per"),
          );

          if ($this->dosis->insertar($datosNuevoDosis)) {
              $this->session->set_flashdata("confirmacion","DOSIS INSERTADO EXITOSAMENTE.");
          } else {
            $this->session->set_flashdata("error","ERROR AL PROCESAR, INTENTE NUEVAMENTE.");
          }
          redirect("dosiss/index");

        }
        function procesarEliminacion($id_dos){
            if($this->dosis->eliminar($id_dos)){
              redirect("dosiss/index");
            }else{
              echo "Error al eliminar";
            }
    }
    }//cierre de la clase
 ?>
