<?php
    //crear una clase
     class Vacunas extends CI_Controller{
          //contructor
          public function __construct(){
            //llamamos al contructor
          parent::__construct();
          //carga el modelo
          $this->load->model("vacuna");
        }
public function index(){

  $data["listadoVacunas"]=$this->vacuna->consultarTodos();
  $this->load->view("header");
  $this->load->view("vacunas/index",$data);
  $this->load->view("footer");
}

public function index2(){

  $data["listadoVacunas"]=$this->vacuna->consultarTodos();
  $this->load->view("header");
  $this->load->view("vacunas/gestionvacuna",$data);
  $this->load->view("footer");
}


public function listado(){
  $data["listadoVacunas"]=$this->vacuna->consultarTodos();
  $this->load->view("vacunas/index",$data);
}

public function nuevo(){
  $this->load->view("header");
  $this->load->view("vacunas/nuevo");
  $this->load->view("footer");
}
public function editar($id_vac){
  $data["vacuna"]=$this->vacuna->consultarPorId($id_vac);
  $this->load->view("header");
  $this->load->view("vacunas/editar",$data);
  $this->load->view("footer");
}
public function procesarActualizacion(){
  $id_vac=$this->input->post("id_vac");
  $datosVacunaEditada=array(
    "nombre_vac"=>$this->input->post("nombre_vac"),
    "tipo_vac"=>$this->input->post("tipo_vac"),
    "mili_vac"=>$this->input->post("mili_vac")
  );
//logica de negocio para subir la fotografia del cliente
  $this->load->library("upload");//carga de la libreria de subida de
  $nombreTemporal="foto_vacuna_".time()."_".rand(1,5000);
  $config["file_name"]=$nombreTemporal;
  $config["upload_path"]=APPPATH.'../uploads/vacunas/';
  $config["allowed_types"]="jpeg|jpg|png";
  $config["max_size"]=2*1024;//2MB
  $this->upload->initialize($config);
  //codigo para subir el archivo y guardar el nombre en la BDD
  if($this->upload->do_upload("foto_vac")){
    $dataSubida=$this->upload->data();
    $datosVacunaEditada["foto_vac"]=$dataSubida["file_name"];
  }


  if($this->vacuna->actualizar($id_vac,$datosVacunaEditada)){
    redirect("vacunas/index");
  }else{
    echo "ERROR AL ACTUALIZAR";
  }
}
public function guardarVacuna(){
      $datosNuevaVacuna=array(
        "nombre_vac"=>$this->input->post("nombre_vac"),
        "tipo_vac"=>$this->input->post("tipo_vac"),
        "mili_vac"=>$this->input->post("mili_vac")
      );
      //logica de negocio para subir la fotografia del cliente
        $this->load->library("upload");//carga de la libreria de subida de
        $nombreTemporal="foto_vacuna_".time()."_".rand(1,5000);
        $config["file_name"]=$nombreTemporal;
        $config["upload_path"]=APPPATH.'../uploads/vacunas/';
        $config["allowed_types"]="jpeg|jpg|png";
        $config["max_size"]=2*1024;//2MB
        $this->upload->initialize($config);
        //codigo para subir el archivo y guardar el nombre en la BDD
        if($this->upload->do_upload("foto_vac")){
          $dataSubida=$this->upload->data();
          $datosNuevaVacuna["foto_vac"]=$dataSubida["file_name"];
        }
      if ($this->vacuna->insertar($datosNuevaVacuna)) {
        $this->session->set_flashdata('confirmacion',"Cliente insertado exitosamente.");
      }else {
        $this->session->set_flashdata('error',"Error al procesar, intente nuevamente.");
      }
      redirect("vacunas/index");
    }


    public function procesarEliminacion($id_vac){
        if($this->vacuna->eliminar($id_vac)){
          redirect("Vacunas/index");
      }else{
        echo "Error al Eliminar";
      }

  }


}
 ?>
