<?php
    //crear una clase
     class Generos extends CI_Controller{
          //contructor
          public function __construct(){
            //llamamos al contructor
          parent::__construct();
          //carga el modelo
          $this->load->model("genero");
        }
public function index(){
  //muestra los datos en el index
  //LOS DATOS LLEGAN A LA VISTA
  $data["listadoGeneros"]=$this->genero->consultarTodos();
  $this->load->view("header");
  $this->load->view("generos/index",$data);
  $this->load->view("footer");
}
public function nuevo(){
  $this->load->view("header");
  $this->load->view("generos/nuevo");
  $this->load->view("footer");
}
public function editar($id_gen){
  $data["genero"]=$this->genero->consultarPorId($id_gen);
  $this->load->view("header");
  $this->load->view("generos/editar",$data);
  $this->load->view("footer");
}
public function procesarActualizacion(){
  $id_gen=$this->input->post("id_gen");
  $datosGeneroEditado=array(
    "id_gen"=>$this->input->post("id_gen"),
    "nombre_gen"=>$this->input->post("nombre_gen"),
    "descripcion_gen"=>$this->input->post("descripcion_gen")
  );
// //logica de negocio para subir la fotografia del cliente
//   $this->load->library("upload");//carga de la libreria de subida de
//   $nombreTemporal="foto_cliente_".time()."_".rand(1,5000);
//   $config["file_name"]=$nombreTemporal;
//   $config["upload_path"]=APPPATH.'../uploads/clientes/';
//   $config["allowed_types"]="jpeg|jpg|png";
//   $config["max_size"]=2*1024;//2MB
//   $this->upload->initialize($config);
//   //codigo para subir el archivo y guardar el nombre en la BDD
//   if($this->upload->do_upload("foto_cli")){
//     $dataSubida=$this->upload->data();
//     $datosClienteEditado["foto_cli"]=$dataSubida["file_name"];
//   }


  if($this->genero->actualizar($id_gen,$datosGeneroEditado)){
    redirect("generos/index");
  }else{
    echo "ERROR AL ACTUALIZAR";
  }
}
public function guardarGenero(){
      $datosNuevoGenero=array(
        "id_gen"=>$this->input->post("id_gen"),
        "nombre_gen"=>$this->input->post("nombre_gen"),
        "descripcion_gen"=>$this->input->post("descripcion_gen")
      );
      // //logica de negocio para subir la fotografia del cliente
      //   $this->load->library("upload");//carga de la libreria de subida de
      //   $nombreTemporal="foto_cliente_".time()."_".rand(1,5000);
      //   $config["file_name"]=$nombreTemporal;
      //   $config["upload_path"]=APPPATH.'../uploads/clientes/';
      //   $config["allowed_types"]="jpeg|jpg|png";
      //   $config["max_size"]=2*1024;//2MB
      //   $this->upload->initialize($config);
      //   //codigo para subir el archivo y guardar el nombre en la BDD
      //   if($this->upload->do_upload("foto_cli")){
      //     $dataSubida=$this->upload->data();
      //     $datosNuevoCliente["foto_cli"]=$dataSubida["file_name"];
      //  }
      if ($this->genero->insertar($datosNuevoGenero)) {
        $this->session->set_flashdata('confirmacion',"Genero insertado exitosamente.");
      }else {
        $this->session->set_flashdata('error',"Error al procesar, intente nuevamente.");
      }
      redirect("generos/index");
    }


    public function procesarEliminacion($id_gen){
        if($this->genero->eliminar($id_gen)){
          redirect("generos/index");
      }else{
        echo "Error al Eliminar";
      }

  }


}
 ?>
