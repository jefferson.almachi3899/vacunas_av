<?php
      class Usuarios extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("usuario");
            $this->load->model("perfil");


        }
//NUEVA LINEA 6 DE JULIO
        public function index(){
          $data["listadoPerfiles"]=$this->perfil->consultarTodos();
          $data["listadoUsuarios"]=$this->usuario->obtenerTodos();
          $this->load->view("header");
          $this->load->view("usuarios/index");
          $this->load->view("footer");
        }
        public function index1(){
          $data["listadoPerfiles"]=$this->perfil->consultarTodos();
          $data["listadoUsuarios"]=$this->usuario->obtenerTodos();
          $this->load->view("header");
          $this->load->view("usuarios/index1",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $data["listadoPerfiles"]=$this->perfil->consultarTodos();
          $data["listadoUsuarios"]=$this->usuario->obtenerTodos();
          $this->load->view("header");
          $this->load->view("usuarios/nuevo",$data);
          $this->load->view("footer");
        }

        public function editar($id_usu){
          $data["listadoPerfiles"]=$this->perfil->consultarTodos();
          $data["usuario"]=$this->usuario->obtenerPorId($id_usu);
          $this->load->view("header");
          $this->load->view("usuarios/editar",$data);
          $this->load->view("footer");
        }


        public function listado(){
          $data["listadoPerfiles"]=$this->perfil->consultarTodos();
          $data["listadoUsuarios"]=$this->usuario->obtenerTodos();
          $this->load->view("usuarios/listado",$data);
        }


    public function insertarUsuario(){
        $data["listadoPerfiles"]=$this->perfil->consultarTodos();
  $data=array(
    "apellido_usu"=>$this->input->post("apellido_usu"),
    "nombre_usu"=>$this->input->post("nombre_usu"),
    "email_usu"=>$this->input->post("email_usu"),
    "password_usu"=>$this->input->post("password_usu"),
    "fk_id_perfil"=>$this->input->post("fk_id_perfil")

  );

  if($this->usuario->insertarUsuario($data)){
    $this->session->set_flashdata("confirmacion","Credenciales Creadas exitosamente. Inicia Sesion Por favor");


  }else{
    $this->session->set_flashdata("error","Error al procesar, intente nuevamente.");
  }
  redirect("usuarios/index1");
}
public function eliminarUsuario(){
    $id_usu=$this->input->post("id_usu");
    if($this->usuario->eliminar($id_usu)){
      echo json_encode(array("respuesta"=>"ok"));
    }else{
      echo json_encode(array("respuesta"=>"error"));
    }
}



public function actualizarUsuarioAjax(){
    $data["listadoPerfiles"]=$this->perfil->consultarTodos();
    $id_usu=$this->input->post("id_usu");
    $data=array(
        "apellido_usu"=>$this->input->post("apellido_usu"),
        "nombre_usu"=>$this->input->post("nombre_usu"),
        "email_usu"=>$this->input->post("email_usu"),
        "fk_id_perfil"=>$this->input->post("fk_id_perfil")
    );
    if($this->usuario->actualizar($data,$id_usu)){
        echo json_encode(array("respuesta"=>"ok"));
    }else{
        echo json_encode(array("respuesta"=>"error"));
    }
}



      }//cierre de la clase
 ?>
