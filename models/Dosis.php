<?php

    //constructor
    class Dosis extends CI_Model{
      //funcion constructor
        public function __construct(){
            parent:: __construct();
        }

        //funcion para insetar datos
        public function insertar($datos){
            return $this->db->insert('dosis',$datos);
        }

        public function actualizar($id_dos,$datos){
          $this->db->where("id_dos",$id_dos);
            return $this->db->update("dosis",$datos);
        }

        public function consultarPorId($id_dos){
          $this->db->where("id_dos",$id_dos);
          $this->db->join("vacuna","vacuna.id_vac=dosis.fk_id_vac");
          $this->db->join("persona","persona.id_per=dosis.fk_id_per");
            $dosis=$this->db->get('dosis');
            if ($dosis->num_rows()>0) {
                return $dosis->row();
            } else {
                return false;
            }

        }

        public function consultarTodos(){
          $this->db->join("vacuna","vacuna.id_vac=dosis.fk_id_vac");
          $this->db->join("persona","persona.id_per=dosis.fk_id_per");
            $listadoDosiss=$this->db->get('dosis');
            if ($listadoDosiss->num_rows()>0) {

                return $listadoDosiss;
            } else {
                return false;
            }
        }

        public function eliminar($id_dos){
        $this->db->where("id_dos",$id_dos);
        return $this->db->delete("dosis");
    }
    


  }   //fin llave

 ?>
