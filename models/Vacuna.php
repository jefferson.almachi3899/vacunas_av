<?php
//consultar y insertar
// programacion orientado objetos
// extends es una herencia  clase cliente
 class Vacuna extends CI_Model{
  //metodo contructor definir el contructor de la clase cliente
  // la primera funcion llamamos al contructor no retorna nada
  public function __construct(){
    // llamamos al contructor
    parent::__construct();
  }
//la segunda funcion para insertar los datos si tiene un retorno
//los datos viene de la vista
public function insertar($datos){
//array para ingresar datos
//nombre, conjunto de datos llamando a la base de datos
  return $this->db->insert('vacuna',$datos);
}
//funcion para actualizar
public function actualizar($id_vac,$datos){
  $this->db->where("id_vac",$id_vac);
  return $this->db->update("vacuna",$datos);
}
//permita sacar el detalle de un clientes
public function consultarPorId($id_vac){
  $this->db->where("id_vac",$id_vac);
  $vacuna=$this->db->get("vacuna");
  if($vacuna->num_rows()>0){
      return $vacuna->row();//cuendo si existe clientes
    }else{
      return false;// cuando no hay clientes
    }
  }
//la tercera funcion para consultar todos los clientes no tiene parametros
public function consultarTodos(){
//nombre de la tabla para traer los datos de la tabla cliente
    $listadoVacunas=$this->db->get('vacuna');
    // un condicion  para consultar los datos.
    //me da el numero de filas de los datos
    if ($listadoVacunas->num_rows()>0) {
      //cuando si hay datos
      return $listadoVacunas;
    }else{
      //cuando no hay datos
      return false;
    }
}
public  function eliminar($id_vac){
  $this->db->where("id_vac",$id_vac);
  return $this->db->delete("vacuna");

}//cierre de la funcion eliminar


// public function obtenerClientesPorEstado($estado){
//   $this->db->where("estado_cli",$estado);
//   $clientes=$this->db->get("vacuna");
//   if ($clientes->num_rows()>0) {
//     return $clientes;
//   }else {
//     return false;
//   }
// }
}//cierre de la clase
 ?>
