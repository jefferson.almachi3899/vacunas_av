<center>
  <div class="row">
    <div class="col-md-10" style="">
      <b style="color:#7485e1;"><h3>Ingresar datos de generos</h3></b>
      <form  style="border:solid; width:80%; "class=""  method="post" action="<?php echo site_url(); ?>/Generos/guardarGenero" id="frm_nuevo_genero" enctype="multipart/form-data" >
      <br>
      <br>
      <label  for="">NOMBRE:</label>
      <br>
      <input class="form-control" style="width:80%; "type="text" name="nombre_gen"  id="nombre_gen" placeholder="Ingrese el nombre del genero " value="" required>
      <br>
      <label  for="">DESCRIPCION:</label>
      <br>
      <input class="form-control" style="width:80%;" type="text" name="descripcion_gen" id="descripcion_gen" placeholder="Ingrese una descripcion del genero" value=""required>
       <br>



<br>
<!-- <br>
<label for="">FOTOGRAFIA</label>
<input type="file" name="foto_cli" accept="image/*" id="foto_cli" value="">
<br>
<br> -->
<br>

<button class="btn btn-success" type="submit" name="button"> <i class="fa fa-save"></i>Guardar</button>
<a class="btn-success" href="<?php echo site_url(); ?>/generos/index">
<i class="fa fa-times"></i></a>

<br>
<br>



  </form>
    </div>

  </div>

</center>

<script type="text/javascript">
  $("#frm_nuevo_genero").validate({
    rules:{

                nombre_gen:{
                  letras:true,
                  required:true
                },
                descripcion_gen:{
                  letras:true,
                  required:true
                }
            },
    messages:{

          nombre_gen:{
            letras:"No se permite ingresar numeros o caracteres",
            required:"Por favor ingrese un nombre"
          },
          descripcion_gen:{
            letras:"No se permite ingresar numeros o caracteres",
            required:"Por favor ingrese una descripcion"
          }
    }
  });

</script>
<!-- <script type="text/javascript">
  $("#foto_cli").fileinput({
    allowedFileExtensions:["jpeg","jpg","png"],
    dropZoneEnabled:true,
    language:"es"
  });

</script> -->
