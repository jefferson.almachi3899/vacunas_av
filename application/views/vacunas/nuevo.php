<center>
  <div class="row">
    <div class="col-md-12" style="">
      <b style="color:#7485e1;"><h3>Ingresar los Datos de la Nueva Vacuna</h3></b>
      <form  style="border:solid; width:80%; "class=""  method="post" action="<?php echo site_url(); ?>/Vacunas/guardarVacuna" id="frm_nuevo_vacuna" enctype="multipart/form-data" >
      <br>
      <br>
      <label  for="">Nombre de La Vacuna:</label>
      <br>
      <input class="form-control" style="width:80%;" type="text" name="nombre_vac" id="nombre_vac" placeholder="Ingrese el apellido......" value=""required>
       <br>
      <label  for="">Tipo de Vacuna:</label>
      <br>
      <input class="form-control"  style="width:80%;" type="text" name="tipo_vac" id="tipo_vac"  placeholder="Ingrese el nombre......"value=""required>
      <br>
      <label  for="">Militraje:</label>
      <br>
      <input class="form-control"  style="width:80%;" type="text" name="mili_vac" id="mili_vac"  placeholder="Ingrese el número......"value=""required>
<br>
<br>
<label for="">FOTOGRAFIA</label>
<input type="file" name="foto_vac" accept="image/*" id="foto_vac" value="">
<br>
<br>
<br>

<button class="btn btn-success" type="submit" name="button">
<i class="fa fa-save"></i>
  Guardar</button>
<a class="btn-success" href="<?php echo site_url(); ?>/vacunas/index">
<i class="fa fa-times"></i>
</a>

<br>
<br>



  </form>
    </div>

  </div>

</center>

<script type="text/javascript">
  $("#frm_nuevo_vacuna").validate({
    rules:{
                fk_id_pais:{
                  required:true
                },

                identificacion_cli:{
                  required:true,
                  minlength:10,
                  maxlength:10,
                  digits:true
                },
                apellido_cli:{
                  letras:true,
                  required:true
                }
            },
    messages:{
        fk_id_pais:{
          required:"Por favor seleccione el pais"
        },
          identificacion_cli:{
            required:"Por favor ingrese el numero de cedula",
            minlength:"La cédula debe tener minimo 10 digitos",
            maxlength:"La cédula debe tener maximo 10 digitos",
            digits:"La cédula solo acepta números"
          },
          apellido_cli:{
            letras:"No se permite ingresar numeros o caracteres",
            required:"Por favor ingrese el apellido"
          }
    }
  });

</script>
<script type="text/javascript">
  $("#foto_vac").fileinput({
    allowedFileExtensions:["jpeg","jpg","png"],
    dropZoneEnabled:true,
    language:"es"
  });

</script>
