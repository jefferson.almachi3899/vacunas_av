<?php
//consultar y insertar
// programacion orientado objetos
// extends es una herencia  clase cliente
 class Genero extends CI_Model{
  //metodo contructor definir el contructor de la clase cliente
  // la primera funcion llamamos al contructor no retorna nada
  public function __construct(){
    // llamamos al contructor
    parent::__construct();
  }
//la segunda funcion para insertar los datos si tiene un retorno
//los datos viene de la vista
public function insertar($datos){
//array para ingresar datos
//nombre, conjunto de datos llamando a la base de datos
  return $this->db->insert('genero',$datos);
}
//funcion para actualizar
public function actualizar($id_gen,$datos){
  $this->db->where("id_gen",$id_gen);
  return $this->db->update("genero",$datos);
}
//permita sacar el detalle de un clientes
public function consultarPorId($id_gen){
  $this->db->where("id_gen",$id_gen);
  $genero=$this->db->get("genero");
  if($genero->num_rows()>0){
      return $genero->row();//cuendo si existe generos
    }else{
      return false;// cuando no hay generos
    }
  }
//la tercera funcion para consultar todos los clientes no tiene parametros
public function consultarTodos(){
//nombre de la tabla para traer los datos de la tabla cliente
    $listadoGeneros=$this->db->get('genero');
    // un condicion  para consultar los datos.
    //me da el numero de filas de los datos
    if ($listadoGeneros->num_rows()>0) {
      //cuando si hay datos
      return $listadoGeneros;
    }else{
      //cuando no hay datos
      return false;
    }
}
public  function eliminar($id_gen){
  $this->db->where("id_gen",$id_gen);
  return $this->db->delete("genero");

}//cierre de la funcion eliminar


// public function obtenerGeneroPorEstado($estado){
//   $this->db->where("estado_cli",$estado);
//   $clientes=$this->db->get("cliente");
//   if ($clientes->num_rows()>0) {
//     return $clientes;
//   }else {
//     return false;
//   }
// }
}//cierre de la clase
 ?>
