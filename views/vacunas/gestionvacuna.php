<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<br>
<div class="row text-center">
  <h1>Gestion de Vacunas</h1>
</div>
<hr>
<div class="row text-center">
  <div class="col-md-4"></div>
  <div class="col-md-4">
    <button type="button" name="button" class="btn btn-primary"
     onclick="cargarListadoVacunas();" title="Actualizar">
     <i class="fa fa-refresh"></i>
    </button>
     <!-- Trigger the modal with a button -->
     <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalNuevaVacuna">
       <i class="fa fa-plus-circle"></i>
     </button>
  </div>

   <div class="col-md-4"></div>
</div>

<div class="row">
  <div class="col-md-12"
  id="contenedor-listado-vacunas">
    <i class="fa fa-spin fa-lg fa-spinner"></i>
    Consultando Datos
  </div>
</div>

<!-- Modal -->
<div id="modalNuevaVacuna"
style="z-index:9999 !important;"
 class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-users"></i> Nueva Vacuna</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="contenedor-nueva-vacuna">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
    function abrirFormulario(){
      // alert("ok...");
      $("#contenedor-nueva-vacuna")
      .html('<i class="fa fa-spin fa-lg fa-spinner"></i>');
      $("#contenedor-nueva-vacuna")
      .load("<?php echo site_url('vacunas/nuevo'); ?>");
      $("#modalNuevaVacuna").modal("show");
    }
</script>

<!-- Modal -->
<div id="modalNuevaVacuna"
style="z-index:9999 !important;"
 class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-users"></i> Nueva Vacuna</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="contenedor-formulario-nueva">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    function abrirFormularioEditar(id_usu){
      // alert("ok...");
      $("#contenedor-formulario-editar")
      .html('<i class="fa fa-spin fa-lg fa-spinner"></i>');
      $("#contenedor-formulario-editar")
      .load("<?php echo site_url('usuarios/editar'); ?>/"+id_usu);
      $("#modalEditarUsuario").modal("show");
    }
</script>

<script type="text/javascript">
function cargarListadoVacunas() {
  $("#contenedor-listado-vacunas").html('<i class="fa fa-spin fa-lg fa-spinner"></i>Cargando')
  $("#contenedor-listado-vacunas").load("<?php echo site_url();?>/vacunas/listado")
  cargarListadoVacunas();
}
</script>
