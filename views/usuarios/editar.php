<br>
<center>
  <b><h2>Editar Usuarios</h2></b>
</center>
<hr>
<br>
</center>


<form class=""
action="<?php echo site_url('usuarios/actualizarUsuarioAjax'); ?>"
method="post"
id="frm_actualizar_usuario">

    <input type="hidden" name="id_usu" id="id_usu"
    value="<?php echo $usuario->id_usu; ?>">

    <label for="">APELLIDO:</label><br>
    <input type="text" name="apellido_usu"
    value="<?php echo $usuario->apellido_usu; ?>"
    id="apellido_usu" class="form-control" required> <br>

    <label for="">NOMBRE:</label><br>
    <input type="text" name="nombre_usu"
    value="<?php echo $usuario->nombre_usu; ?>"
    id="nombre_usu" class="form-control"> <br>

    <label for="">EMAIL:</label><br>
    <input type="text" name="email_usu"
    value="<?php echo $usuario->email_usu; ?>"
    id="email_usu" class="form-control"> <br>

    <label for="">PERFIL</label>
    <select class="form-control" name="fk_id_perfil" id="fk_id_perfil"
    required >
        <option value="">--Seleccione un perfil--</option>
        <?php if ($listadoPerfiles): ?>
          <?php foreach ($listadoPerfiles->result() as $paisTemporal): ?>
              <option value="<?php echo $paisTemporal->id_perfil; ?>">
                <?php echo $paisTemporal->nombre_perfil; ?>
              </option>
          <?php endforeach; ?>
        <?php endif; ?>
    </select>
    <br>


    <script type="text/javascript">
        $("#fk_id_perfil").val("<?php echo $usuario->fk_id_perfil; ?>");
    </script>
    <br>
    <button type="button" onclick="actualizar();" name="button"
    class="btn btn-success">
      <i class="fa fa-pen"></i> Actualizar
    </button>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url('usuarios/index1'); ?>"class="btn btn-warning " align="center"><i class="fa fa-times"></i>&nbsp;CANCELAR</a>
    </a>
</form>


<script type="text/javascript">

function actualizar(){

    $.ajax({
      url:$("#frm_actualizar_usuario").prop("action"),
      data:$("#frm_actualizar_usuario").serialize(),
      type:"post",
      success:function(data){
        cargarListadoUsuarios();
        $("#modalEditarUsuario").modal("hide");
        $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
        $('.modal-backdrop').remove();//eliminamos el backdrop del modal
        var objetoJson=JSON.parse(data);
        if(objetoJson.respuesta=="ok" || objetoJson.respuesta=="OK"){
          iziToast.success({
               title: 'CONFIRMACIÓN',
               message: 'Actualización Exitosa',
               position: 'topRight',
             });
        }else{
          iziToast.error({
               title: 'ERROR',
               message: 'Error al procesar',
               position: 'topRight',
             });
        }

      }
    });
}

</script>
