<br>
<center>
  <b><h2>Nuevo Uuarios</h2></b>
</center>
<hr>
<br>
</center>

<form class=""
action="<?php echo site_url('usuarios/insertarUsuario'); ?>"
method="post"
id="frm_nuevo_usuario">
<label for="">APELLIDO:</label>
<br>
<input type="text" name="apellido_usu"
id="apellido_usu" class="form-control"> <br>

<label for="">NOMBRE:</label>
<br>
<input type="text" name="nombre_usu"
id="nombre_usu" class="form-control"> <br>

<label for="">EMAIL:</label>
<br>
<input type="text" name="email_usu"
id="email_usu" class="form-control"> <br>

<label for="">CONTRASEÑA:</label>
<br>
<input type="password" name="password_usu"
id="password_usu" class="form-control"> <br>

<label for="">CONFIRME LA CONTRASEÑA:</label>
<br>
<input type="password" name="password_confirmada"
id="password_confirmada" class="form-control"> <br>

<label for="">PERFIL</label>
<select class="form-control" name="fk_id_perfil" id="fk_id_perfil"
required >
    <option value="">--Seleccione un perfil--</option>
    <?php if ($listadoPerfiles): ?>
      <?php foreach ($listadoPerfiles->result() as $paisTemporal): ?>
          <option value="<?php echo $paisTemporal->id_perfil; ?>">
            <?php echo $paisTemporal->nombre_perfil; ?>
          </option>
      <?php endforeach; ?>
    <?php endif; ?>
</select>
<br>
<button type="submit" name="button"
class="btn btn-success">
  <i class="fa fa-save"></i> Guardar
</button>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="<?php echo site_url('usuarios/index1'); ?>"class="btn btn-warning " align="center"><i class="fa fa-times"></i>&nbsp;CANCELAR</a>
</a>

</form>
