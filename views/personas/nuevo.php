
<form  action="<?php echo site_url(); ?>/personas/guardarPersona" method="post" id="frm_nuevo_per"  enctype="multipart/form-data">

<br>
<br>
<!-- <div class="row"> -->
<table class="table table-success table-striped">
  <div class="col-12 grid-margin">
    <div class="card">
      <div class="card-body text-center">
        <h4 class="card-title">INGRESAR NUEVA PERSONA</h4>
        <br>
        <form class="form-sample">

    <!-- inicio -->
    <div class="row">
  <div class="col-md-12">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label">Cedula: </label>
      <div class="col-sm-4">
        <input type="number"  name="identificacion_per" id="identificacion_per" class="form-control" />
      </div>
    </div>
  </div>
  <br><br>
  <div class="col-md-12">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label">Nombre: </label>
      <div class="col-sm-4">
        <input type="text"  name="nombre_per" id= "nombre_per" class="form-control" placeholder="" />
      </div>
    </div>
  </div>
  <br><br>
  <div class="col-md-12">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label">Apellido: </label>
      <div class="col-sm-4">
        <input type="text"  name="apellido_per" id= "apellido_per" class="form-control" placeholder="" />
      </div>
    </div>
  </div>
  <br><br>
  <div class="col-md-12">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label">Genero</label>
      <div class="col-sm-4">
        <select class="form-control" name="fk_id_gen" id="fk_id_gen" required>
          <option value="">----Seleccione género-----</option>
          <?php if ($listadoGeneros): ?>
            <?php foreach ($listadoGeneros->result() as $generoTemporal): ?>
              <option value="<?php echo $generoTemporal->id_gen; ?>"><?php echo $generoTemporal->nombre_gen; ?></option>
            <?php endforeach; ?>
          <?php endif; ?>

        </select>
      </div>
    </div>
  </div>
</div>
    <!-- fin -->
  <br><br>
          <label for="">FOTOGRAFIA</label>
          <input type="file" name="foto_per"
          accept="image/*"
          id="foto_per" value="">
        
    <div class="row">
    <div class="col-md-12 mt-4 mb-4 text-center">
      <button type="submit" class="btn btn-success " align="center"><i class="fa fa-save"></i>Guardar</b></button>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="<?php echo site_url('personas/index'); ?>"class="btn btn-warning " align="center"><i class="fa fa-times"></i>&nbsp;CANCELAR</a>
      </a>
    </div>
  </div>

</form>
</div>
</div>
</div>
</form>
<script type="text/javascript">
  $('#frm_nuevo_per').validate({
      rules:{
        fk_id_genero:{
          required:true,

        },
        cedula_per:{
          required:true,
          minlength:10,
          maxlength:10,
          digits:true,
        },
        nombre_per:{
          letras:true,
          required:true,



        }
      },
      messages:{
        fk_id_genero:{
          required:"Por favor ingrese el genero",

        },
        cedula_per:{
          required:"Por favor ngrese la cedula",
          minlength:"La cedula debe tener 10 dijitos",
          maxlength:"la cedula debe tener 10 numeros",
          digits:"Solo acepta numeros"
        },
        nombre_per:{
          letras:"Nombre incorrecto",
          required:"Por favo ingrese el Nombre"



        }
      }

  });
</script>
<!-- </script> -->
<script type="text/javascript">
  $("#foto_per").fileinput({
    allowedFileExtension:["jpeg","jpg","png"],
    dropZoneEnabled:true,
    language:"es"
  });
</script>
<!-- <script type="text/javascript">
$('#foto_per').fileinput({
       theme: 'fa5',
       language: 'es',
       uploadUrl: 'https://www.tooltyp.com/8-beneficios-de-usar-imagenes-en-nuestros-sitios-web/',
       allowedFileExtensions: ['jpg', 'png', 'jpeg']
   });

</script> -->
