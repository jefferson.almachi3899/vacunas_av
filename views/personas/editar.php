<br>
<form  action="<?php echo site_url(); ?>/personas/procesarActualizacion" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_per"  id="id_per" value="<?php echo $persona->id_per; ?>">
<div class="col-12 grid-margin">
  <div class="card">
    <div class="card-body text-center">
      <h4 class="card-title">EDITAR PERSONAS</h4>
      <form class="form-sample">

<!-- <div class="row"> -->
<table class="table table-success table-striped">
  <div class="col-md-12">

    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-sm-4 col-form-label">Identificacion:</label>
        <div class="col-sm-4">
           <input type="text" class="form-control" value="<?php echo $persona->identificacion_per; ?>" type="text" name="identificacion_per" id="identificacion_per" value="" placeholder="Por favor ingrese la identificacion">
        </div>
        <br><br>
      </div>
      <div class="form-group row">
        <label class="col-sm-4 col-form-label">Nombre:</label>
        <div class="col-sm-4">
          <input type="text" class="form-control" value="<?php echo $persona->nombre_per; ?>" type="text" name="nombre_per" id= "nombre_per"value="" placeholder="Ingrese su nombre">
        </div>
        <br><br>
      </div>
      <div class="form-group row">
        <label class="col-sm-4 col-form-label">Apellido:</label>
        <div class="col-sm-4">
          <input type="text" class="form-control" value="<?php echo $persona->apellido_per; ?>" type="text" name="apellido_per" id= "apellido_per"value="" placeholder="Ingrese su apellido">
        </div>
        <br><br>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-sm-4 col-form-label">Genero</label>
        <div class="col-sm-4">
          <select class="form-control" name="fk_id_gen" id="fk_id_gen" required>
            <option value="">----Seleccione género-----</option>
            <?php if ($listadoGeneros): ?>
              <?php foreach ($listadoGeneros->result() as $generoTemporal): ?>
                <option value="<?php echo $generoTemporal->id_gen; ?>"><?php echo $generoTemporal->nombre_gen; ?></option>
              <?php endforeach; ?>
            <?php endif; ?>

          </select>
        </div>
      </div>
    </div>
  <br>
  <label for="">FOTOGRAFIA</label>
  <input type="file" name="foto_per"
  accept="image/*"
  id="foto_per" value="<?php echo $persona->foto_per; ?>">
  </div>
    <br>
  

         <button type="submit" class="btn btn-success" align="center"><i class="fa fa-save"></i>&nbsp;ACTUALIZAR</button>
         &nbsp;&nbsp;&nbsp;
           <a href="<?php echo site_url('personas/index'); ?>"class="btn btn-warning " align="center"><i class="fa fa-times"></i>&nbsp;CANCELAR</a>



</form>
</div>
</div>
</div>
</form>
<script type="text/javascript">
$("#fk_id_gen").val("<?php  echo $persona->fk_id_gen; ?>");
$("#foto_per").val("<?php echo $persona->foto_per; ?>");
</script>
<script type="text/javascript">
   $("#foto_per").fileinput({
     allowedFileExtensions:["jpeg","jpg","png"],
     msgProcessing:"Procesando..",
     initialPreview:[" <?php echo base_url();?>/uploads/personas/<?php echo $persona->foto_per ?>"  ],
     initialPreviewAsData: true,
     initialPreviewFileType: 'image',
     dropZoneEnabled:true,
     language:"es",
     msgPlaceholder:"<?php echo $persona->foto_per ?>"
   });


</script>
