<br>
<form  action="<?php echo site_url(); ?>/dosiss/procesarActualizacion" method="post" enctype="multipart/form-data">

<div class="col-12 grid-margin">
<div class="card">
  <div class="card-body text-center">
    <h4 class="card-title">EDITAR DATOS DE DOSIS</h4>
    <form class="form-sample">

<br>
<div class="row">
<div class="col-md-12">
  <div class="form-group row">
    <label class="col-sm-3 col-form-label">Vacuna:</label>
    <div class="col-sm-5">
      <!-- required para campos obligatorios -->
      <select class="form-control" name="fk_id_vac" id="fk_id_vac" required>
        <option value="">----Seleccione una vacuna-----</option>
        <?php if ($listadoVacunas): ?>
          <?php foreach ($listadoVacunas->result() as $vacunaTemporal): ?>
            <option value="<?php echo $vacunaTemporal->id_vac; ?>"><?php echo $vacunaTemporal->nombre_vac; ?></option>
          <?php endforeach; ?>
        <?php endif; ?>

      </select>
    </div>
  </div>
</div>
<br><br>
<div class="col-md-12">
  <div class="form-group row">
    <label class="col-sm-3 col-form-label">Persona </label>
    <div class="col-sm-5">
      <!-- required para campos obligatorios -->
      <select class="form-control" name="fk_id_per" id="fk_id_per" required>
        <option value="">----Seleccione una persona-----</option>
        <?php if ($listadoPersonas): ?>
          <?php foreach ($listadoPersonas->result() as $personaTemporal): ?>
            <option value="<?php echo $personaTemporal->id_per; ?>"><?php echo $personaTemporal->nombre_per; ?></option>
          <?php endforeach; ?>
        <?php endif; ?>

      </select>
    </div>
  </div>
</div>
</div>
<br>
<div class="row">
<div class="col-md-12">
  <div class="form-group row">
    <label class="col-sm-3 col-form-label">Lugar de la Dosis:</label>
    <div class="col-sm-5">
      <input type="text"  value="<?php echo $dosis->lugar_dos; ?>" name="lugar_dos" id="lugar_dos" class="form-control" />
    </div>
  </div>
</div>
<br><br>
<div class="col-md-12">
  <div class="form-group row">
    <label class="col-sm-3 col-form-label">Numero de dosis: </label>
    <div class="col-sm-9">
      <select class="form-control" type="text" style="width:60% "name="numero_dos" id="numero_dos">
      <option value="">--Seleccione--</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
    </select>
    </div>
  </div>
</div>
</div>
<br>
<div class="row">
<div class="col-md-12">
  <div class="form-group row">
    <label class="col-sm-3 col-form-label">Vacunador:</label>
    <div class="col-sm-5">
      <input type="text"  value="<?php echo $dosis->vacunador_dos; ?>" name="vacunador_dos" id= "vacunador_dos" class="form-control" />
    </div>
  </div>
</div>
<br><br>
<div class="col-md-12">
  <div class="form-group row">
    <label class="col-sm-3 col-form-label">Fecha: </label>
    <div class="col-sm-5">
      <input type="date" value="<?php echo $dosis->fecha_dos; ?>" class="form-control" name="fecha_dos" id= "fecha_dos" />
    </div>
  </div>
</div>
</div>
<br><br>
      <button type="submit" class="btn btn-success " align="center"><i class="fa fa-save"></i>&nbsp;ACTUALIZAR</button>
      &nbsp;&nbsp;&nbsp;
        <a href="<?php echo site_url('dosiss/index'); ?>"class="btn btn-warning " align="center"><i class="fa fa-times"></i>&nbsp;CANCELAR</a>

</form>
</div>
</div>
</div>
</form>
<script type="text/javascript">
$("#fk_id_vac").val("<?php  echo $dosis->fk_id_vac; ?>");
$("#fk_id_per").val("<?php  echo $dosis->fk_id_per; ?>");
$("#numero_dos").val("<?php  echo $dosis->numero_dos; ?>");

</script>
