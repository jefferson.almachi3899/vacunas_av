
<br>
<form  action="<?php echo site_url(); ?>/dosiss/guardarDosis" method="post" id="frm_nuevo_dos"  enctype="multipart/form-data">
  <div class="col-12 grid-margin">
  <div class="card">
    <div class="card-body text-center">
      <h4 class="card-title">INGRESAR DATOS DE LA DOSIS</h4>
      <br>
      <form class="form-sample">

<div class="row">
  <div class="col-md-12">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label">Vacuna:</label>
      <div class="col-sm-4">
        <!-- required para campos obligatorios -->
        <select class="form-control" name="fk_id_vac" id="fk_id_vac" required>
          <option value="">----Seleccioneun vacuna-----</option>
          <?php if ($listadoVacunas): ?>
            <?php foreach ($listadoVacunas->result() as $vacunaTemporal): ?>
              <option value="<?php echo $vacunaTemporal->id_vac; ?>"><?php echo $vacunaTemporal->tipo_vac; ?></option>
            <?php endforeach; ?>
          <?php endif; ?>

        </select>
      </div>
    </div>
  </div>
  <br><br>
  <div class="col-md-12">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label">Persona </label>
      <div class="col-sm-4">
        <!-- required para campos obligatorios -->
        <select class="form-control" name="fk_id_per" id="fk_id_per" required>
          <option value="">----Seleccione una Persona-----</option>
          <?php if ($listadoPersonas): ?>
            <?php foreach ($listadoPersonas->result() as $personaTemporal): ?>
              <option value="<?php echo $personaTemporal->id_per; ?>"><?php echo $personaTemporal->identificacion_per; ?></option>
            <?php endforeach; ?>
          <?php endif; ?>

        </select>
      </div>
    </div>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-12">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label">Lugar de la Dosis:</label>
      <div class="col-sm-4">
        <input type="text"  name="lugar_dos" id="lugar_dos" class="form-control" />
      </div>
    </div>
  </div>
  <br><br>
  <div class="col-md-12">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label">Numero de dosis: </label>
      <div class="col-sm-6">
        <select class="form-control" type="text" style="width:60% "name="numero_dos" id="numero_dos">
        <option value="">--Seleccione--</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
      </select>
      </div>
    </div>
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-12">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label">Vacunador:</label>
      <div class="col-sm-4">
        <input type="text"  name="vacunador_dos" id= "vacunador_dos" class="form-control" />
      </div>
    </div>
  </div>
  <br><br>
  <div class="col-md-12">
    <div class="form-group row">
      <label class="col-sm-3 col-form-label">Fecha: </label>
      <div class="col-sm-4">
        <input type="date" class="form-control" name="fecha_dos" id= "fecha_dos" />
      </div>
    </div>
  </div>
</div>
<br><br>
    <div class="row">
      <div class="col-md-12 mt-4 mb-4 text-center">
        <button type="submit" class="btn btn-success " align="center"><i class="fa fa-save"></i>Guardar</b></button>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="<?php echo site_url('dosiss/index'); ?>"class="btn btn-warning " align="center"><i class="fa fa-times"></i>&nbsp;CANCELAR</a>
        </a>
      </div>
  </div>

</form>
</div>
</div>
</div>
</form>
<script type="text/javascript">
  $('#frm_nuevo_dos').validate({
      rules:{
        fk_id_va:{
          required:true,

        },
        fk_id_persona:{
          required:true,

        },
        lugar_dos:{
          letras:true,
          required:true,
        },
        numero_dos:{
          required:true,

        },
        vacunador_dos:{
          letras:true,
          required:true,

        },
        fecha_dos:{
            required:true,
          date:true,
        }
      },
      messages:{
        fk_id_va:{
          required:"Por favor ingrese la vacuna",

        },
        fk_id_persona:{
          required:"Por favor ingrese la persona",

        },
        lugar_dos:{
          letras:"Nombre incorrecto",
          required:"Por favo ingrese el nombre de la dosis",
        },
        numero_dos:{
          required:"Por favo ingrese el numero de dosis"
        },
        vacunador_dos:{
          letras:"Apellido incorrecto",
          required:"Por favo ingrese el nombre de la persona quien lo vacuno"

        },
        fecha_dos:{
          required:"ingrese una fecha",
          date:"Fecha incorrecto"

        }
      }

  });
</script>
</script>
<script type="text/javascript">
  $("#foto_cli").fileinput({
    allowedFileExtension:["jpeg","jpg","png"],
    dropZoneEnabled:true,
    language:"es"
  });
</script>
